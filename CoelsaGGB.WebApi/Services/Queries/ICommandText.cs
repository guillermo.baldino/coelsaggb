﻿namespace Dapper.WebApi.Services.Queries
{
    public interface ICommandText
    {
        string GetContacts { get; }
        string GetContactById { get; }
        string AddContact { get; }
        string UpdateContact { get; }
        string RemoveContact { get; }
        string GetContactByIdSp { get; }
    }
}
