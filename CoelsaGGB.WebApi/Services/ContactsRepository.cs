﻿using Dapper.WebApi.Models;
using Dapper.WebApi.Services.Queries;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Linq;

namespace Dapper.WebApi.Services
{
	public class ContactsRepository : BaseRepository, IContactsRepository
    {
        private readonly ICommandText _commandText;
        public int _total = 1;

        public ContactsRepository(IConfiguration configuration, ICommandText commandText) : base(configuration)
        {
            _commandText = commandText;

        }

        public async Task<IEnumerable<Contacts>> GetAllContacts()
        {

            return await WithConnection(async conn =>
            {
                var query = await conn.QueryAsync<Contacts>(_commandText.GetContacts);
                return query;
            });

        }

        public async ValueTask<List<Contacts>> GetById(string company, int offset, int limit)
        {
            return await WithConnection(async conn =>
            {
                int pageNo = offset;
                int pageSize = limit;
                int skip = (pageNo - 1) * pageSize;
                var query = await conn.QueryAsync<Contacts>(_commandText.GetContactById, new { Company = company });
                this._total = query.Count();
                var queryOrdenada = query.OrderBy(c => c.IDContacts).Skip(skip).Take(pageSize).ToList();
                return queryOrdenada;
            });

        }

        public async Task AddContact(Contacts entity)
        {
			await WithConnection(async conn =>
			{
				await conn.ExecuteAsync(_commandText.AddContact,
					new { FirsName = entity.FirstName, PhoneNumber = entity.PhoneNumber, LastName = entity.LastName, Company = entity.Company, Email = entity.Email });
			});

			//await WithConnection(async conn =>
			//{
			//	var p = new DynamicParameters();
			//	p.Add("@FirstName", entity.FirstName);
			//             p.Add("@LastName", entity.LastName);
			//             p.Add("@PhoneNumber", entity.PhoneNumber);
			//             p.Add("@Company", entity.Company);
			//             p.Add("@Email", entity.Email);

			//             //await conn.ExecuteAsync("SP_Company_Update", p, commandType: CommandType.StoredProcedure);

			//});

		}
        public async Task UpdateContact(Contacts entity, int IDContacts)
        {
            await WithConnection(async conn =>
            {
                await conn.ExecuteAsync(_commandText.UpdateContact,
                    new { IDContacts = IDContacts, FirsName = entity.FirstName, PhoneNumber = entity.PhoneNumber, LastName = entity.LastName, Company = entity.Company, Email = entity.Email });
            });

        }
        public async Task RemoveContact(int IDContacts)
        {

            await WithConnection(async conn =>
            {
                await conn.ExecuteAsync(_commandText.RemoveContact, new { IDContacts = IDContacts });
            });

        }

		public int total()
		{
            return this._total;
		}
	}
}
