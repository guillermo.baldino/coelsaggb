﻿using Dapper.WebApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dapper.WebApi.Services
{
    public interface IContactsRepository
    {
        ValueTask<List<Contacts>> GetById(string IDContacts, int offset, int limit);
        Task AddContact(Contacts entity);
        Task UpdateContact(Contacts entity, int IDContacts);
        Task RemoveContact(int IDContacts);
        Task<IEnumerable<Contacts>> GetAllContacts();

        int total();
    }
}
