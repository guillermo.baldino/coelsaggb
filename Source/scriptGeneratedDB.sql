USE [master]
GO
/****** Object:  Database [CoelsaGGB]    Script Date: 27/12/2020 14:16:44 ******/
CREATE DATABASE [CoelsaGGB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CoelsaGGB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\CoelsaGGB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'CoelsaGGB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\CoelsaGGB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [CoelsaGGB] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CoelsaGGB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CoelsaGGB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CoelsaGGB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CoelsaGGB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CoelsaGGB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CoelsaGGB] SET ARITHABORT OFF 
GO
ALTER DATABASE [CoelsaGGB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CoelsaGGB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CoelsaGGB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CoelsaGGB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CoelsaGGB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CoelsaGGB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CoelsaGGB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CoelsaGGB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CoelsaGGB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CoelsaGGB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CoelsaGGB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CoelsaGGB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CoelsaGGB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CoelsaGGB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CoelsaGGB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CoelsaGGB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CoelsaGGB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CoelsaGGB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [CoelsaGGB] SET  MULTI_USER 
GO
ALTER DATABASE [CoelsaGGB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CoelsaGGB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CoelsaGGB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CoelsaGGB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [CoelsaGGB] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [CoelsaGGB] SET QUERY_STORE = OFF
GO
USE [CoelsaGGB]
GO
/****** Object:  Table [dbo].[Contacts]    Script Date: 27/12/2020 14:16:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contacts](
	[IDContacts] [int] IDENTITY(1,1) NOT NULL,
	[FirsName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[Company] [nvarchar](50) NULL,
	[Email] [nvarchar](250) NULL,
	[PhoneNumber] [nvarchar](50) NULL,
 CONSTRAINT [PK_Contacts] PRIMARY KEY CLUSTERED 
(
	[IDContacts] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[SP_Company_Update]    Script Date: 27/12/2020 14:16:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_Company_Update]
@FirstName      nvarchar(200), 
@LastName      nvarchar(200), 
@PhoneNumber      nvarchar(200), 
@Company      nvarchar(200), 
@Email      nvarchar(200)
AS 
BEGIN

 BEGIN
  BEGIN TRANSACTION 
  BEGIN TRY
   INSERT INTO Contacts(
	    FirsName,
        PhoneNumber,
		LastName,
		Company,
		Email)
      VALUES (
        @FirstName,
		@LastName,
		@PhoneNumber,
		@Company,
		@Email)



                           
   COMMIT TRAN 
  END TRY
  BEGIN CATCH
   ROLLBACK TRANSACTION
   PRINT 'Se ha producido un error!'
  END CATCH    
 END
 
END
GO
USE [master]
GO
ALTER DATABASE [CoelsaGGB] SET  READ_WRITE 
GO
