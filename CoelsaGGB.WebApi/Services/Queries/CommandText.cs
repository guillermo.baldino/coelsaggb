namespace Dapper.WebApi.Services.Queries
{
    public class CommandText : ICommandText
    {
        public string GetContacts => "Select * from Contacts";
        public string GetContactById => "Select * from Contacts where company = @Company";
        public string AddContact => "Insert into " +
            " [CoelsaGGB].[dbo].[Contacts] (FirsName, PhoneNumber, LastName, Company, Email) " +
            "values (@FirsName, @PhoneNumber, @LastName, @Company, @Email)";
        public string UpdateContact => "Update [CoelsaGGB].[dbo].[Contacts] set " +
            "FirsName = @FirsName, " +
            "PhoneNumber = @PhoneNumber, " +
            "LastName = @LastName, " +
            "Company = @Company, " +
            "Email = @Email " +
            "where IDContacts = @IDContacts";
        public string RemoveContact => "Delete from [CoelsaGGB].[dbo].[Contacts] where IDContacts= @IDContacts";
        public string GetContactByIdSp => "GetContactsId";

    }
}
