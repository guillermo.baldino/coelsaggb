﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace Dapper.WebApi.Models
{
	/// <summary>
	/// A user attached to an account
	/// </summary>
	public class Contacts //: BaseEntity
	{

		//[Key, Identity]
		//[Column("IDContacts", TypeName = "int")]
		//[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		//[Required]
		public int? IDContacts { get; set; } // int, not null

		[Required]
		[StringLength(50)]
		public string FirstName { get; set; }


		[Required]
		[StringLength(50)]
		public string LastName { get; set; }


		[StringLength(50)]
		public string Company { get; set; }

		[StringLength(250)]
		[Required(ErrorMessage = "Email is missing."), EmailAddress(ErrorMessage = "Email is not valid.")]
		public string Email { get; set; }

		[StringLength(50)]
		[DataType(DataType.PhoneNumber)]
		[Phone]
		public string PhoneNumber { get; set; }
		
	}
}
